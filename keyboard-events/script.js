let boxTop = 200;
let boxLeft = 200; 
let box = document.getElementById("box")
document.addEventListener('keydown', (event) => {
    const keyName = event.key
    if (keyName == 'ArrowUp'){
        boxTop -= 10
        box.style.transform = 'rotate(90deg)'        
    }
    if (keyName == 'ArrowDown'){
        boxTop += 10
        box.style.transform = 'rotate(-90deg)'
    }
    if (keyName == 'ArrowRight'){
        boxLeft += 10
        box.style.transform = 'rotate(180deg)'
    }
    if (keyName == 'ArrowLeft'){
        boxLeft -= 10
        box.style.transform = 'rotate(0deg)'
    }
    box.style.top = boxTop + "px";
    box.style.left = boxLeft + "px";
})
