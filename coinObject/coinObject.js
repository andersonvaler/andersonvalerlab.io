const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random()*2)
    },
    toString: function() {
        return this.state == 0 ? 'Heads' : 'Tails'
    },
    toHTML: function() {
        const image = document.createElement('img');
        if (this.state == 0){
            image.src = './images/coroa.jpg' 
        }else{
            image.src = './images/cara.jpg'
        }
        return image;
    }
};
 
function display20Flips() {
    const results = [];
    let body = document.getElementById('body')
    body.innerHTML += '<br>'
    for(let flip = 0; flip < 20; flip++){
        coin.flip()
        results.push(coin.toString())
        body.innerHTML += JSON.stringify(coin.toString()) + ' '
    }
    body.innerHTML += JSON.stringify(results)
}
 
function display20Images() {
    const results = [];
    let body = document.getElementById('body')
    body.innerHTML += '<br>'
    for(let flip = 0; flip < 20; flip++){
        coin.flip()
        results.push(coin.toString())
        body.appendChild(coin.toHTML())
    }
    body.innerHTML += JSON.stringify(results)
}
 
display20Flips()
display20Images()