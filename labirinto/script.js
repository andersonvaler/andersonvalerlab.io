const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]
let win = false
const labirinto = document.getElementById('labirinto')
//Construir o labirinto
let idElement = 0
for (let linha = 0 ; linha < map.length ; linha++){
    let container = document.createElement('div')
    container.className = 'container'
    labirinto.appendChild(container)
    for(let coluna = 0 ; coluna < map[linha].length ; coluna++){  
        if(map[linha][coluna] == 'W'){
            //parede
            let wall = document.createElement('div')
            container.appendChild(wall)
            wall.className = 'wall'
            wall.id = `${idElement}`
        } else if (map[linha][coluna] == ' '){
            //caminho
            let way = document.createElement('div')
            container.appendChild(way)
            way.className = 'way'
            way.id = `${idElement}`
        }else if(map[linha][coluna] == 'S'){
            // inicio
            let start = document.createElement('div')
            container.appendChild(start)
            start.className = 'start'
            start.id = `${idElement}`
        }else{
            //final
            let end = document.createElement('div')
            container.appendChild(end)
            end.className = 'end'
            end.id = `${idElement}`
        }  
        idElement++
    }
}
//Criar o player
let playerPosition = 189
let player = document.createElement('div')
player.className = 'player'
let position = document.getElementById(`${playerPosition}`)
position.appendChild(player)
//Movimentação
document.addEventListener('keydown', (event) => {
    const keyName = event.key
    if (keyName == 'ArrowUp' && valida(-21)){
        position = document.getElementById(`${playerPosition-=21}`)
        position.appendChild(player)
    }
    if (keyName == 'ArrowDown' && valida(21)){
        position = document.getElementById(`${playerPosition+=21}`)
        position.appendChild(player)
    }
    if (keyName == 'ArrowRight' && valida(1)){
        position = document.getElementById(`${playerPosition+=1}`)
        position.appendChild(player)
    }
    if (keyName == 'ArrowLeft' && valida(-1) && position.className != 'start'){
        position = document.getElementById(`${playerPosition-=1}`)
        position.appendChild(player)
    }  
    winner()
})
  
//Verificação
function valida(id){
    let atual = playerPosition 
    let nextPosition = document.getElementById(`${atual + id}`)
    if (nextPosition.className == 'wall'){
        return (false)
    }else{
       return (true)
    }
}

function winner(){
    if (position.id == '188'){
        setInterval(() => { 
            position = document.getElementById('189')
            position.appendChild(player)
            window.alert('Parabens, você conseguiu!')
        }, 100);
    }
}