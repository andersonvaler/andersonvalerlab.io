document.getElementById('countButton').onclick = function(){
  let typedText = document.getElementById('textInput').value
  typedText = typedText.toLowerCase()
  typedText = typedText.replace(/[^a-z'\s]+/g, "")
  let lettersDiv = document.getElementById("lettersDiv")
  let wordsDiv = document.getElementById('wordsDiv')
  lettersDiv.innerHTML = ''
  wordsDiv.innerHTML = ''
  const letterCounts = {}
  let words = {}
  let wordCount = {}

  for(let i = 0; i < typedText.length; i++){
    let currentLetter = typedText[i]
    if (letterCounts[currentLetter] === undefined) {
      letterCounts[currentLetter] = 1
   } else { 
      letterCounts[currentLetter]++
   }
  }

  words = typedText.split(/\s/)

  for(let i2 = 0; i2 < words.length; i2++){
    let currentWord = words[i2]
    if (wordCount[currentWord] === undefined) {
      wordCount[currentWord] = 1
   } else { 
      wordCount[currentWord]++
   }
  }
 
  for (let letter in letterCounts) { 
    let span = document.createElement("span"); 
    let textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter]) 
    span.appendChild(textContent)
    lettersDiv.appendChild(span)
 }

  for (let word in wordCount) { 
    let span2 = document.createElement("span"); 
    let textWords = document.createTextNode('"' + word + "\": " + wordCount[word] + " ") 
    span2.appendChild(textWords)
    wordsDiv.appendChild(span2)
  }
}
