//01. Retornar os números de 1 a 20.
function oneThroughTwenty() {
    let out = []
    for (let count = 1; count <= 20; count++) {
        out += count + ' '
    }
    console.log('01-Números de 1 a 20 --> ' + out)
    console.log('')
}

oneThroughTwenty() //call function oneThroughTwenty

//02. Retornar os números pares de 1 a 20.
function evensToTwenty() {
    let out = []
    for (let count = 1; count <= 20; count++) {
        if (count % 2 == 0) {
            out += count + ' '
        }
    }
    console.log('02-Pares de 1 a 20 --> ' + out)
    console.log('')
    return out
}

evensToTwenty() //call function evensToTwenty

//03. Retornar os números ímpares de 1 a 20.
function oddsToTwenty() {
    let out = []
    for (let count = 1; count <= 20; count++) {
        if (count % 2 == 1) {
            out += count + ' '
        }
    }
    console.log('03-Ímpares de 1 a 20 --> ' + out)
    console.log('')
}

oddsToTwenty() //call function oddsToTwenty

//4. Retornar os múltiplos de 5 até 100.
function multiplesOfFive() {
    let out = []
    for (let count = 1; count <= 100; count++) {
        if (count % 5 == 0) {
            out += count + ' '
        }
    }
    console.log('04-Multiplos de 5 de 1 a 100 --> ' + out)
    console.log('')
}

multiplesOfFive() //call function multiplesOfFive

//5. Retornar todos os números até 100 que forem quadrados perfeitos. 
function squareNumbers() {
    let out = []
    for (let count = 1; count <= 10; count++) {
        out += count ** 2 + ' '
    }
    console.log('05-Quadrados perfeitos de 1 a 100 --> ' + out)
    console.log('')
}

squareNumbers() //call function squareNumbers

//6. Retornar os números contando de trás para frente de 20 até 1.
function countingBackwards() {
    let out = []
    for (let count = 20; count >= 1; count--) {
        out += count + ' '
    }
    console.log('06-Números de 20 a 1 --> ' + out)
    console.log('')
}

countingBackwards() //call function countingBackwards

//7. Retornar os números pares de 20 até 1.
function evenNumbersBackwards() {
    let out = []
    for (let count = 20; count >= 1; count--) {
        if (count % 2 == 0) {
            out += count + ' '
        }
    }
    console.log('07-Pares de 20 a 1 --> ' + out)
    console.log('')
}

evenNumbersBackwards() //call function evenNumbersBackwards

//8. Retornar os números ímpares de 20 até 1.
function oddNumbersBackwards() {
    let out = []
    for (let count = 20; count >= 1; count--) {
        if (count % 2 == 1) {
            out += count + ' '
        }
    }
    console.log('08-Ímpares de 20 a 1 --> ' + out)
    console.log('')
}

oddNumbersBackwards() //call function oddNumbersBackwards

//9. Retornar os múltiplos de 5 contando de trás para frente a partir de 100. 
function multiplesOfFiveBackwards() {
    let out = []
    for (let count = 100; count >= 1; count--) {
        if (count % 5 == 0) {
            out += count + ' '
        }
    }
    console.log('09-Múltiplos de 5 de 100 a 1 --> ' + out)
    console.log('')
}

multiplesOfFiveBackwards() //call function multiplesOfFiveBackwards

//10. Retornar os quadrados perfeitos contando de trás para frente a partir de 100. (100, 81, 64, …, 4, 1)
function squareNumbersBackwards() {
    let out = []
    for (let count = 10; count >= 1; count--) {
        out += count ** 2 + ' '
    }
    console.log('10-Quadrados perfeitos de 100 a 1 --> ' + out)
    console.log('')
}

squareNumbersBackwards() //call function squareNumbersBackwards