Array.prototype.newForEach = function (callbackFunction, thisArg = this){
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        callbackFunction(currentValue, index, thisArg)
    }
}

Array.prototype.newMap = function (callbackFunction, thisArg = this){
    let result = []
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        result.push(callbackFunction(currentValue, index, thisArg))
    }
    return result
}

Array.prototype.newSome = function (callbackFunction, thisArg = this){
    let result = false
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        if (callbackFunction(currentValue, index, thisArg) == true){
            result = true 
            break
        } 
    }
    return result
}

Array.prototype.newFind = function (callbackFunction, thisArg = this){
    let result = undefined
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        if (callbackFunction(currentValue, index, thisArg) == true){
            result = currentValue 
            break
        } 
    }
    return result
}

Array.prototype.newFindIndex = function (callbackFunction, thisArg = this){
    let result = -1
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        if (callbackFunction(currentValue) == true){
            result = index
            break
        } 
    }
    return Number(result)
}

Array.prototype.newEvery = function (callbackFunction, thisArg = this){
    let result = true
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        if (callbackFunction(currentValue, index, thisArg) == false || callbackFunction(currentValue, index, thisArg) == undefined ){
            result = false 
            break
        } 
    }
    return result
}

Array.prototype.newFilter = function (callbackFunction, thisArg = this){
    let result = []
    for (let index = 0; index < thisArg.length; index++){
        let currentValue = thisArg[index]
        if (callbackFunction(currentValue, index, thisArg) == true){
            result.push(currentValue) 
        }
    }
    return result
}

Array.prototype.newIncludes = function(searchElement, fromIndex, thisArg = this){
    let result = false
    if(fromIndex == undefined){
        fromIndex = 0
    }
    for(fromIndex; fromIndex < thisArg.length; fromIndex++){
        if (thisArg[fromIndex] == searchElement){
            result = true
            break
        }  
    }
    return result
}

Array.prototype.newIndexOf = function(searchElement, fromIndex, thisArg = this){
    let result = -1
    if (fromIndex == undefined){
        fromIndex = 0
    }
    for(fromIndex; fromIndex < thisArg.length; fromIndex++){
        if (thisArg[fromIndex] == searchElement){
            result = fromIndex
            break
        }
    }
    return result
}

Array.prototype.newJoin = function (separator, thisArg = this){
    let result = ''
    if (separator == undefined){
        separator = ','
    }
    for (let index = 0; index < thisArg.length; index++ ){
        if (index == thisArg.length - 1){
            result += thisArg[index]
            break
        }
        result += thisArg[index] + separator
    }
    return result
}

Array.prototype.newSlice = function(startIndex, endIndex, thisArg = this){
    let result = []
    if (startIndex == undefined){
        startIndex = 0
    }
    if (endIndex == undefined){
        endIndex = thisArg.length
    }
    for(startIndex; startIndex < endIndex; startIndex++){
        result.push(thisArg[startIndex])
    }
    return result
}