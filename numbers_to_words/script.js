function numbersToWords(){
  let count = 0
  let ateCem = ['']
  const unidade = [
    'um', 'dois', 'tres', 'quatro', 'cinco',
    'seis', 'sete', 'oito', 'nove'
  ]
  const dez = [ 
    'dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze',
    'dezesseis', 'dezessete', 'dezoito', 'dezenove'
  ] 
  const dezena = [
    'vinte', 'trinta', 'quarenta', 'cinquenta', 'sessenta',
    'setenta', 'oitenta', 'noventa', 'cem'
  ]
  const centena = [
    'cento', 'duzentos', 'trezentos', 'quatrocentos', 'quinhentos',
    'seiscentos', 'setecentos', 'oitocentos', 'novecentos', 'mil'
  ]
  for(let i = 0; i < 20; i++){
    if (i < 9){
      ateCem.push(` ${unidade[i]}`)
    }else if (i < 19){
      ateCem.push(` ${dez[count]}`)
      count++
    }else if(i == 19){
      for(let countDez = 0; countDez < 8; countDez++){
        for(let c = 0; c < unidade.length + 1; c++){
          if (c == 0){
            ateCem.push(` ${dezena[countDez]}`)
          }else{
            ateCem.push(` ${dezena[countDez]} e ${unidade[c-1]}`) 
          }      
        }
        if (countDez == 7){
          ateCem.push(' ' + dezena[countDez+1]) 
        }      
      }    
    }   
  }
  for (let i = 0; i < 9; i++){
    for(let c = 0; c < 99; c++){
      ateCem.push(` ${centena[i]} e ${ateCem[c+1]}`)
    }
    ateCem.push(' ' + centena[i+1])
  }
return(ateCem)
}

let resultados = document.getElementById('resultados')
let exec = numbersToWords()
resultados.innerHTML = numbersToWords()
