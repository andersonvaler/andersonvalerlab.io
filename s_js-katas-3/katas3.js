const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

function kata1() {
  let nums = []
  for (let i = 1; i <= 25; i++){
    nums.push(' ' + i)
  }
  return(nums)
}

function kata2() {
  let nums = []
  for(let i = 25; i > 0; i--){
    nums.push(' ' + i)
  }
  return(nums)
}

function kata3() {
  let nums = []
  for (let i = 1; i <= 25; i++){
    nums.push(' ' + -i)
  }
  return(nums)
}

function kata4() {
  let nums = []
  for(let i = 25; i > 0; i--){
    nums.push(' ' + -i)
  }
  return(nums)
}

function kata5() {
  let nums = []
  for(let i = 25; i >= -25; i--){
    if (i % 2 == 1 || i % 2 == -1){
      nums.push(' ' + i) 
    } 
  }
  return(nums)
}

function kata6() {
  let nums = []
  for (let i = 1; i <= 100; i++){
    if (i % 3 == 0){
      nums.push(' ' + i) 
    } 
  }
  return(nums)
}

function kata7() {
  let nums = []
  for (let i = 1; i <= 100; i++){
    if (i % 7 == 0){
      nums.push(' ' + i) 
    } 
  }
  return(nums)
}

function kata8() {
  let nums = []
  for (let i = 100; i > 0; i--){
    if (i % 7 == 0 || i % 3 == 0){
      nums.push(' ' + i)
    }
  }
  return(nums)
}

function kata9() {
  let nums = []
  for (let i = 1; i <= 100; i++){
    if (i % 5 == 0 && i % 2 == 1){
      nums.push(' ' + i) 
    } 
  }
  return(nums)
}

function kata10() {
  let nums = []
  for (let i = 0; i < sampleArray.length; i++){
    nums.push(' ' + sampleArray[i])
  }
  return(nums)
}

function kata11() {
  let nums = []
  for (let i = 0; i < sampleArray.length; i++){
    if (sampleArray[i] % 2 == 0){
      nums.push(' ' + sampleArray[i] ) 
    } 
  }
  return(nums)
}

function kata12() {
  let nums = []
  for (let i = 0; i < sampleArray.length; i++){
    if (sampleArray[i] % 2 == 1){
      nums.push(' ' + sampleArray[i]) 
    } 
  }
  return(nums)
}

function kata13() {
  let nums = []
  for (let i = 0; i < sampleArray.length; i++){
    if (sampleArray[i] % 8 == 0){
      nums.push(sampleArray[i] + ' ') 
    } 
  }
  return(nums)
}

function kata14() {
  let nums = []
  for (let i = 0; i < sampleArray.length; i++){
    nums.push(' ' + sampleArray[i] ** 2) 
  }
  return(nums)
}

function kata15() {
  let nums = 0
  for (let i = 1; i <= 20; i++){
    nums += i
  }
  return(nums)
}

function kata16() {
  let nums = 0
  for (let i = 0; i < sampleArray.length; i++){
    nums += sampleArray[i] 
  }
  return(nums)
}

function kata17() {
  let nums = sampleArray[0]
  for (let i = 0; i < sampleArray.length; i++){
    if(sampleArray[i] < nums)
    nums = sampleArray[i] 
  }
  return(nums)
}

function kata18() {
  let nums = sampleArray[0]
  for (let i = 0; i < sampleArray.length; i++){
    if(sampleArray[i] > nums)
    nums = sampleArray[i] 
  }
  return(nums)
}

function kataBonus1() {
  let main = document.getElementById('main')
  let divKb1 = document.createElement('div')
  let title1 = document.createElement('h2')
  title1.innerHTML = 'Kata bonus 1'
  divKb1.appendChild(title1)
  divKb1.id = 'b1'
  main.appendChild(divKb1)
  for(let i = 0; i < 20; i++){
    divKb1.innerHTML += ('<div style="background-color: grey; width: 100px; height: 20px; margin: 2px;"></div>')
  }
}

function kataBonus2() {
  let main = document.getElementById('main')
  let divKb2 = document.createElement('div')
  let title2 = document.createElement('h2')
  title2.innerHTML = 'Kata bonus 2'
  divKb2.appendChild(title2)
  divKb2.id = 'b2'
  let width = 105
  main.appendChild(divKb2)
  for(let i = 0; i < 20; i++){
    divKb2.innerHTML += (`<div style="background-color: grey; width: ${width}px; height: 20px; margin: 2px;"></div>`)
    width += 5
  }
}

function kataBonus3() {
  let main = document.getElementById('main')
  let divKb3 = document.createElement('div')
  main.appendChild(divKb3)
  let title3 = document.createElement('h2')
  title3.innerHTML = 'Kata bonus 3'
  divKb3.appendChild(title3)
  divKb3.id = 'b3'
  for(let i = 0; i < 20; i++){
    let width = sampleArray[i]
    divKb3.innerHTML += (`<div style="background-color: grey; width: ${width}px; height: 20px; margin: 2px;"></div>`)
  }
}

function kataBonus4() {
  let main = document.getElementById('main')
  let divKb4 = document.createElement('div')
  main.appendChild(divKb4)
  let title4 = document.createElement('h2')
  title4.innerHTML = 'Kata bonus 4'
  divKb4.appendChild(title4)
  divKb4.id = 'b4'
  let color = 'red'
  for(let i = 0; i < 20; i++){
    let width = sampleArray[i]
    if (i % 2 == 0){
      color = 'red'
    }else{
      color = 'grey'
    }
    divKb4.innerHTML += (`<div style="background-color: ${color}; width: ${width}px; height: 20px; margin: 2px;"></div>`)
  }
}

function kataBonus5() {
  let main = document.getElementById('main')
  let divKb5 = document.createElement('div')
  main.appendChild(divKb5)
  let title5 = document.createElement('h2')
  title5.innerHTML = 'Kata bonus 5'
  divKb5.appendChild(title5)
  divKb5.id = 'b5'
  let color = 'red'
  for(let i = 0; i < 20; i++){
    let width = sampleArray[i]
    if (width % 2 == 0){
      color = 'red'
    }else{
      color = 'grey'
    }
    divKb5.innerHTML += (`<div style="background-color: ${color}; width: ${width}px; height: 20px; margin: 2px;"></div>`)
  }
}

let main = document.getElementById('main')
let kata01 = document.createElement('div')
let tk1 = document.createElement('h2')
main.appendChild(kata01)
kata01.appendChild(tk1)
tk1.innerHTML = 'Kata1'
kata01.innerHTML += kata1()

let kata02 = document.createElement('div')
let tk2 = document.createElement('h2')
main.appendChild(kata02)
kata02.appendChild(tk2)
tk2.innerHTML = 'Kata2'
kata02.innerHTML += kata2()

let kata03 = document.createElement('div')
let tk3 = document.createElement('h2')
main.appendChild(kata03)
kata03.appendChild(tk3)
tk3.innerHTML = 'Kata3'
kata03.innerHTML += kata3()

let kata04 = document.createElement('div')
let tk4 = document.createElement('h2')
main.appendChild(kata04)
kata04.appendChild(tk4)
tk4.innerHTML = 'Kata4'
kata04.innerHTML += kata4()

let kata05 = document.createElement('div')
let tk5 = document.createElement('h2')
main.appendChild(kata05)
kata05.appendChild(tk5)
tk5.innerHTML = 'Kata5'
kata05.innerHTML += kata5()

let kata06 = document.createElement('div')
let tk6 = document.createElement('h2')
main.appendChild(kata06)
kata06.appendChild(tk6)
tk6.innerHTML = 'Kata6'
kata06.innerHTML += kata6()

let kata07 = document.createElement('div')
let tk7 = document.createElement('h2')
main.appendChild(kata07)
kata07.appendChild(tk7)
tk7.innerHTML = 'Kata7'
kata07.innerHTML += kata7()

let kata08 = document.createElement('div')
let tk8 = document.createElement('h2')
main.appendChild(kata08)
kata08.appendChild(tk8)
tk8.innerHTML = 'Kata8'
kata08.innerHTML += kata8()

let kata09 = document.createElement('div')
let tk9 = document.createElement('h2')
main.appendChild(kata09)
kata09.appendChild(tk9)
tk9.innerHTML = 'Kata9'
kata09.innerHTML += kata9()

let kata010 = document.createElement('div')
let tk010 = document.createElement('h2')
main.appendChild(kata010)
kata010.appendChild(tk010)
tk010.innerHTML = 'Kata10'
kata010.innerHTML += kata10()

let kata011 = document.createElement('div')
let tk11 = document.createElement('h2')
main.appendChild(kata011)
kata011.appendChild(tk11)
tk11.innerHTML = 'Kata11'
kata011.innerHTML += kata11()

let kata012 = document.createElement('div')
let tk12 = document.createElement('h2')
main.appendChild(kata012)
kata012.appendChild(tk12)
tk12.innerHTML = 'Kata12'
kata012.innerHTML += kata12()

let kata013 = document.createElement('div')
let tk13 = document.createElement('h2')
main.appendChild(kata013)
kata013.appendChild(tk13)
tk13.innerHTML = 'Kata13'
kata013.innerHTML += kata13()

let kata014 = document.createElement('div')
let tk14 = document.createElement('h2')
main.appendChild(kata014)
kata014.appendChild(tk14)
tk14.innerHTML = 'Kata14'
kata014.innerHTML += kata14()

let kata015 = document.createElement('div')
let tk15 = document.createElement('h2')
main.appendChild(kata015)
kata015.appendChild(tk15)
tk15.innerHTML = 'Kata15'
kata015.innerHTML += kata15()

let kata016 = document.createElement('div')
let tk16 = document.createElement('h2')
main.appendChild(kata016)
kata016.appendChild(tk16)
tk16.innerHTML = 'Kata16'
kata016.innerHTML += kata16()

let kata017 = document.createElement('div')
let tk17 = document.createElement('h2')
main.appendChild(kata017)
kata017.appendChild(tk17)
tk17.innerHTML = 'Kata17'
kata017.innerHTML += kata17()

let kata018 = document.createElement('div')
let tk18 = document.createElement('h2')
main.appendChild(kata018)
kata018.appendChild(tk18)
tk18.innerHTML = 'Kata18'
kata018.innerHTML += kata18()

let bonus = document.createElement('div')

kataBonus1()
kataBonus2()
kataBonus3()
kataBonus4()
kataBonus5()