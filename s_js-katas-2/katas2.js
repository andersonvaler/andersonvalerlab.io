
function add(num1, num2){
    return (num1 + num2)
}

function multiply(num1, num2){
    let res = 0
    for (let i = 0; i < num1; i++){
        res = add(num2, res)
    }
    return (res)
}

function power(num, exp){
    let res = 1
    for (let i = 0; i < exp; i++){
        res = multiply(num, res)   
    }
    return (res)
}

function factorial(num){
    let res = num
    for (let i = num - 1; i >= 1; i--){
        res = multiply(res, i)            
    }
    return (res)
}

function fibonacci(num){
    let seqFib = [0,0,1]
    let i = 1
    do{
        seqFib[add(i,2)] = add(seqFib[i],seqFib[add(i,1)])
        i = add(i,1)
    }while(i < num)
    if (num === 1){
        return(0)
    }else{
        return(seqFib[i])
    } 
}