const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

let main = document.getElementById('main')

function createElementsOnPage(name, kataFunction){
    let div = document.createElement('div')
    div.className = name
    let tittle = document.createElement('h2')
    tittle.innerHTML = name
    main.appendChild(div)
    div.appendChild(tittle)
    div.innerHTML += JSON.stringify(kataFunction())
}

function kata1() {//Escreva uma função que retorna um array com as cidades em 'gotCitiesCSV'
  let arr = gotCitiesCSV.split(",")
  return(arr)
  
}
createElementsOnPage('Kata01', kata1)

function kata2() {//Escreva uma função que retorna um array das palavras na frase contida em 'bestThing'
  let arr = bestThing.split(" ")
  return(arr)
}

createElementsOnPage('Kata02', kata2)

function kata3() {//Escreva uma função que retorna uma string separada por 
  //ponto-e-vírgulas em vez das vírgulas de 'gotCitiesCSV'
  let str = gotCitiesCSV.replace(/,/g, ";")
  return(str)
}

createElementsOnPage('Kata03', kata3)

function kata4() {//Escreva uma função que retorne uma string CSV (separada por vírgulas) de 'lotrCitiesArray'
  let str = lotrCitiesArray.toString()
  return(str)
}

createElementsOnPage('Kata04', kata4)

function kata5() {//Escreva uma função que retorna um array com as 5 primeiras cidades de 'lotrCitiesArray'
  let arr = lotrCitiesArray.slice(0,5)
  return(arr)
}

createElementsOnPage('Kata05', kata5)

function kata6() { //Escreva uma função que retorna um array com as 5 últimas cidades de 'lotrCitiesArray'
  let arr = lotrCitiesArray.slice(lotrCitiesArray.length-5,lotrCitiesArray.length)
  return(arr)
}

createElementsOnPage('Kata06', kata6)

function kata7() { //Escreva uma função que retorna um array contendo da 3ª a 5ª cidades de 'lotrCitiesArray'
  let arr = lotrCitiesArray.slice(2,5)
  return(arr)
}

createElementsOnPage('Kata07', kata7)

function kata8() { //Escreva uma função que use 'splice' 
//para remover 'Rohan' de 'lotrCitiesArray' e retorne o novo 'lotrCitiesArray' modificado
  lotrCitiesArray.splice(2,1)
  return(lotrCitiesArray)
}

createElementsOnPage('Kata08', kata8)

function kata9() {//Escreva uma função que use 'splice' para remover todas as cidades 
//depois de 'Dead Marshes' de 'lotrCitiesArray' e retorne o novo 'lotrCitiesArray' modificado
  lotrCitiesArray.splice(5,2)
  return(lotrCitiesArray)
}

createElementsOnPage('Kata09', kata9)

function kata10() { //Escreva uma função que use 'splice' para adicionar 'Rohan' de volta ao 'lotrCitiesArray' 
//logo depois de 'Gondor' e retorne o novo 'lotrCitiesArray' modificado
  lotrCitiesArray.splice(2, 0,'Rohan')
  return(lotrCitiesArray)
}

createElementsOnPage('Kata10', kata10)

function kata11() { //Escreva uma função que use 'splice' para renomear 'Dead Marshes' para 'Deadest Marshes' 
//em 'lotrCitiesArray' e retorne o novo 'lotrCitiesArray' modificado
  lotrCitiesArray.splice(5, 1,'Deadest Marshes')
  return(lotrCitiesArray)
}

createElementsOnPage('Kata11', kata11)

function kata12() { //Escreva uma função que usa 'slice' para retornar uma string 
//com os primeiros 14 caracteres de 'bestThing'
  let str = bestThing.slice (0,14)
  return(str)
}

createElementsOnPage('Kata12', kata12)

function kata13() { //Escreva uma função que usa 'slice' para retornar uma string 
//com os 12 últimos caracteres de 'bestThing'
  let str = bestThing.slice(bestThing.length-12,bestThing.length)
  return(str)
}

createElementsOnPage('Kata13', kata13)

function kata14() { //Escreva uma função que usa 'slice' para retornar uma string com os caracteres 
//entre as posições 23 e 38 de 'bestThing' (ou seja, 'booleano é par')
  let str = bestThing.slice(23,38)
  return(str)
}

createElementsOnPage('Kata14', kata14)

function kata15() { //Escreva uma função que faz exatamente a mesma coisa que a #13 
  //mas use o método 'substring' em vez de 'slice'
  let str = bestThing.substring(bestThing.length-12,bestThing.lengt)
  return(str)
}

createElementsOnPage('Kata15', kata15)

function kata16(){ //Escreva uma função que faça exatamente a mesma coisa que o #14 
  //mas use o método 'substring' em vez de 'slice'
  let str = bestThing.substring(23,38)
  return(str)
}

createElementsOnPage('Kata16', kata16)

function kata17(){ //Escreva uma função que use 'pop' para remover a 
//última cidade de 'lotrCitiesArray e retorne o novo array
  let str = lotrCitiesArray.pop()
  return(lotrCitiesArray)
}

createElementsOnPage('Kata17', kata17)

function kata18() { //Escreva uma função que usa 'push' para adicionar de volta, no final do array, 
//a cidade de 'lotrCitiesArray' que foi removida no #17 e retorne o novo array
  lotrCitiesArray.push('Deadest Marshes')
  return(lotrCitiesArray)
}

createElementsOnPage('Kata18', kata18)

function kata19(){  //Escreva uma função que usa 'shift' para remover a primeira 
//cidade de 'lotrCitiesArray e retorne o novo array
  let str = lotrCitiesArray.shift()
  return(lotrCitiesArray)
}

createElementsOnPage('Kata19', kata19)

function kata20(){ //Escreva uma função que use 'unshift' para adicionar de volta, no começo do array, 
//a cidade de 'lotrCitiesArray' que foi removida no #19 e retorne o novo array
  let str = lotrCitiesArray.unshift('Mordor')
  return(lotrCitiesArray)
}

createElementsOnPage('Kata20', kata20)

//1. Escreva uma função que encontre e retorne o índice de 'only' em 'bestThing'
function kataBonus1(){
  let ind = bestThing.indexOf('only')
  return(ind)
}

createElementsOnPage('Kata Bonus 1', kataBonus1)

//2. Escreva uma função que encontre e retorne o índice da última palavra de 'bestThing'
function kataBonus2(){
  let ind = bestThing.lastIndexOf(' ') + 1
  return(ind)
}

createElementsOnPage('Kata Bonus 2', kataBonus2)

//3. Escreva uma função que encontre e retorne um array de todas as 
//cidades de 'gotCitiesCSV' que tiverem vogais duplicadas ('aa', 'ee', etc.)
function kataBonus3(){
  let arr = gotCitiesCSV.split(',')
  let out = []
  for (let i = 0; i < arr.length; i++){
      if (arr[i].includes('aa') || arr[i].includes('ee') || 
        arr[i].includes('ii') || arr[i].includes('oo') || arr[i].includes('uu')){
           out.push(' ' + arr[i]) 
      }
    }
  return out
}

createElementsOnPage('Kata Bonus 3', kataBonus3)

//4. Escreva uma função que encontre e retorne um array com todas 
//as cidades de 'lotrCitiesArray' que terminem em 'or'
function kataBonus4(){
    let out = []
    for (i in lotrCitiesArray){
        if (lotrCitiesArray[i].lastIndexOf('or') > -1 ){
            out.push(' ' + lotrCitiesArray[i])
        }
    }
    return out
}

createElementsOnPage('Kata Bonus 4', kataBonus4)

//5. Escreva uma função que encontre e retorne um array com 
//todas as palavras de 'bestThing' começando com 'b'
function kataBonus5(){
    let arr = bestThing.split(' ')
    let out = []
    for (i in arr){
        if (arr[i][0] == 'B' || arr[i][0] == 'b'){
            out.push(' ' + arr[i])
        }
    }
    return out
}

createElementsOnPage('Kata Bonus 5', kataBonus5)

//6. Escreva uma função que retorne 'Sim' ou 'Não' se 'lotrCitiesArray' incluir 'Mirkwood'
function kataBonus6(){
    let out = 'Não'
    if (lotrCitiesArray.includes('Mirkwood')){
        out = 'Sim'
    }
    return out
}

createElementsOnPage('Kata Bonus 6', kataBonus6)

//7. Escreva uma função que retorne 'Sim' ou 'Não' se 'lotrCitiesArray' incluir 'Hollywood'
function kataBonus7(){
    let out = 'Não'
    if (lotrCitiesArray.includes('Hollywood')){
        out = 'Sim'
    }
    return out 
}

createElementsOnPage('Kata Bonus 7', kataBonus7)

//8. Escreva uma função que retorne o índice de 'Mirkwood' em 'lotrCitiesArray'
function kataBonus8(){
    return (lotrCitiesArray.indexOf('Mirkwood'))
}

createElementsOnPage('Kata Bonus 8', kataBonus8)

//9. Escreva uma função que encontre e retorne a primeira cidade de 
//'lotrCitiesArray' que tiver mais de uma palavra
function kataBonus9(){
    let out = ''
    for (let i = 0; i < lotrCitiesArray.length; i++){
        if (lotrCitiesArray[i].includes(' ')){
            out = lotrCitiesArray[i]
            i = lotrCitiesArray.length 
        }
    }
    return out  
}

createElementsOnPage('Kata Bonus 9', kataBonus9)

//10. Escreva uma função que inverta a ordem de 'lotrCitiesArray' e retorne o novo array
function kataBonus10(){
    let out = lotrCitiesArray.reverse()
    return out 
}

createElementsOnPage('Kata Bonus 10', kataBonus10)

//11. Escreva uma função que ordene 'lotrCitiesArray' 
//alfabeticamente e retorne o novo array
function kataBonus11(){
    let out = lotrCitiesArray.sort()
    return out  
}

createElementsOnPage('Kata Bonus 11', kataBonus11)

//12. Escreva uma função que ordene 'lotrCitiesArray' pelo número de caracteres 
//em cada cidade (por exemplo, a cidade mais curta aparece primeiro) e retorne o novo array
function kataBonus12(){    
    
}