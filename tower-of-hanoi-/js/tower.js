const tower1 = document.getElementById('tower1');
const tower2 = document.getElementById('tower2');
const tower3 = document.getElementById('tower3');
const status = document.getElementById('status');
const btn = document.getElementById('btn');

let flagFirst = true;
let inMoving = null;
let evtLast = null;
let countMoves = 0;

const error1 = 'Selecinoado torre sem discos!';
const error2 = 'Disco maior em cima do menor!';
const textMoves = 'Número de movimentos: ';
const winner = 'Parabéns! Você finalizou ';
const tryAgain = ' Você pode melhorar!\n Tente completar com 15 movimentos!';
let typeMsg = 'error';


// Verifica se completou com 15 movimentos.
const checkMinMoves = () => {
    return countMoves == 15;
};

//Envio de mensagem ao usuário'
const msgUser = (typeMsg, textMsg) => {
    status.style.color = 'black';
    status.style.fontWeight = 'normal';

    if (typeMsg == 'error') {
        status.style.fontWeight = '900';
        status.style.color = 'red';
    }

    if (textMsg == textMoves) {
        textMsg += `${countMoves}`;
    }

    if (textMsg == winner) {
        status.style.color = 'blue';
        status.style.fontWeight = '900';
        textMsg += `usando ${countMoves} movimentos`;
        if (!checkMinMoves()) {
            textMsg += tryAgain;
        }
        // desabilita os botões de movimento das torres
        tower1.removeEventListener('click', handlerTower);
        tower2.removeEventListener('click', handlerTower);
        tower3.removeEventListener('click', handlerTower);
    }
    status.innerText = textMsg;
}


// Verifica se tem discos na torre que o usuário clicou.
const isNotEmpty = (num) => {
    if (!num) {
        typeMsg = 'error'
        msgUser(typeMsg, error1);
        return false;
    }
    return true;
}


// Verifica se o tamanho do disco
// Se for maior do que está retorna false
const IsLessThanCurrent = (last, actual) => {
    // O iv evitar o null quando não tem elemento na torre
    if (actual && last) {
        let comperActual = actual.id.slice(-1);
        let comperLast = last.id.slice(-1);
        if (comperActual < comperLast) {
            return false;
        }
    }
    return true;
}

// Condição de vitória é quando os 4 elementos estão na torre 3.
const isFinished = (element) => {
    if (element.childElementCount == 4) {
        typeMsg = 'others'
        msgUser(typeMsg, winner);
    }
};


// Tratamento do primeiro clique
const handlerFirstClick = (evtCurr) => {
    let elementCount = evtCurr.childElementCount;
    if (isNotEmpty(elementCount)) {
        flagFirst = false;
        evtLast = evtCurr;
        inMoving = evtCurr.lastElementChild;
        inMoving.style.border = '2px solid white';
    }
};

// Tratamento do segundo clique
const handlerSecondClick = (evtCurr) => {
    let lastpicked = evtCurr.lastElementChild;

    //Verificar se está clicando na mesma torre.
    if (evtLast === evtCurr) {
        flagFirst = true;
        inMoving.style.border = 'none';
        inMoving = null;
        evtLast = null;
        return;
    }

    // Move se Disco for menor do que está na nova torre.
    if (IsLessThanCurrent(inMoving, lastpicked)) {
        flagFirst = true;
        inMoving.style.border = 'none';
        evtCurr.appendChild(inMoving);
        inMoving = null;
        countMoves++;
        typeMsg = 'others';
        msgUser(typeMsg, textMoves);
    } else {
        flagFirst = true;
        inMoving.style.border = 'none';
        inMoving = null;
        typeMsg = 'error';
        msgUser(typeMsg, error2);
    }
}

// Tratamento do clique em qq torre.
const handlerTower = (evt) => {

    let evtCurrent = evt.currentTarget;

    typeMsg = 'others';
    msgUser(typeMsg, textMoves);

    // Se primeiro clique 
    if (flagFirst) {
        handlerFirstClick(evtCurrent);
        evtLast = evtCurrent;
    }
    // Se segundo clique 
    else {
        handlerSecondClick(evtCurrent);
    }

    // Verificar se finalizou
    isFinished(tower3);
};


// Reiniciar
const reload = (evt) => {
    document.location.reload();
}

tower1.addEventListener('click', handlerTower);
tower2.addEventListener('click', handlerTower);
tower3.addEventListener('click', handlerTower);
btn.addEventListener('click', reload);