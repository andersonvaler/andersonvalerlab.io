let pontoUser = document.getElementById('pontoUser')
let pontoComputer = document.getElementById('pontoComputer')
let pontoEmpate = document.getElementById('pontoEmpate')
let rodada = 0
let pUser = 0
let pComputer = 0
let pEmpate = 0
pontoUser.innerText = pUser.toString()
pontoComputer.innerText = pComputer.toString()
pontoEmpate.innerText = pEmpate.toString()

let pc1 = document.getElementById('pc1')
let pc2 = document.getElementById('pc2')
let pc3 = document.getElementById('pc3')
let u1 = document.getElementById('u1')
let u2 = document.getElementById('u2')
let u3 = document.getElementById('u3')
let userGun = document.getElementById('userGun')
let computerGun = document.getElementById('computerGun')
let vencedor = document.getElementById('vencedor')

function apagaEscolha(){
  pc1.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  pc1.style.color = 'rgba(255, 255, 255, 25%)'
  pc2.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  pc2.style.color = 'rgba(255, 255, 255, 25%)'
  pc3.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  pc3.style.color = 'rgba(255, 255, 255, 25%)'
  u1.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  u1.style.color = 'rgba(255, 255, 255, 25%)'
  u2.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  u2.style.color = 'rgba(255, 255, 255, 25%)'
  u3.style.backgroundColor = 'rgba(0, 0, 0, 10%)'
  u3.style.color = 'rgba(255, 255, 255, 25%)'
}

function game(escolha){
    setTimeout(() => {
     let escolhaPc = Math.floor(Math.random() * 3 + 1)
  if (escolhaPc == 1){
    pc1.style.backgroundColor = '#fff'
    pc1.style.color = 'black'
  }else if(escolhaPc == 2){
    pc2.style.backgroundColor = '#fff'
    pc2.style.color = 'black'
  }else{
    pc3.style.backgroundColor = '#fff'
    pc3.style.color = 'black'
  }
  if(escolha == escolhaPc){
    pEmpate++
    pontoEmpate.innerText = pEmpate.toString()
    computerGun.style.border = '2px solid blue'
    userGun.style.border = '2px solid blue'
    vencedor.innerText = 'Empate'
    vencedor.style.color = 'blue'
  }else if ((escolha == 1 && escolhaPc == 3) || (escolha == 2 && escolhaPc == 1) || (escolha == 3 && escolhaPc == 2)){
    pUser++
    pontoUser.innerText = pUser.toString()
    userGun.style.border = '2px solid green'
    computerGun.style.border = '2px solid red'
    vencedor.innerText = 'Você venceu a rodada'
    vencedor.style.color = 'green'
  }else{
    pComputer++
    pontoComputer.innerText = pComputer.toString()
    computerGun.style.border = '2px solid green'
    userGun.style.border = '2px solid red'
    vencedor.innerText = 'Você perdeu a rodada'
    vencedor.style.color = 'red'
  }
  if(pUser < pComputer){
    pontoUser.style.color = 'red'
    pontoComputer.style.color = 'green'
  }else if(pUser > pComputer){
    pontoUser.style.color = 'green'
    pontoComputer.style.color = 'red'
  }else if(pUser == pComputer){
    pontoUser.style.color = 'blue'
    pontoComputer.style.color = 'blue'
  }
  pontoEmpate.style.color = 'blue' 
  }, 200);
  setTimeout(() => {
    apagaEscolha()
  }, 2000);
  if (rodada == 10){
    fim()
  }
  rodada++
}

function ePe(){
  apagaEscolha()
  u1.style.backgroundColor = '#fff'
  u1.style.color = 'black'
  game(1) 
}

function ePa(){
  apagaEscolha()  
  u2.style.backgroundColor = '#fff'
  u2.style.color = 'black'
  game(2)
}

function eTe(){
  apagaEscolha()
  u3.style.backgroundColor = '#fff'
  u3.style.color = 'black'
  game(3)
}

function fim(){
  let placar = document.getElementById('placar')
  placar.innerHTML = ''
  let tFim = document.createElement('h1')
  placar.appendChild(tFim)
  tFim.innerText = 'FIM DE JOGO'
  let placarFinal = document.getElementById('game')
  placarFinal.innerHTML = ''
  let msg = document.createElement('h2')
  placarFinal.appendChild(msg)
  placarFinal.style.textAlign = 'center'
  msg.innerText = 'Placar Final'
  let upf = document.createElement('h3')
  placarFinal.appendChild(upf)
  upf.innerHTML = `Você => ${pUser} pontos`
  let cpf = document.createElement('h3')
  placarFinal.appendChild(cpf)
  cpf.innerText = `Computador => ${pComputer} pontos`
  let emp = document.createElement('h3')
  placarFinal.appendChild(emp)
  emp.innerText = `Empates => ${pEmpate}`
  let msgFinal = document.createElement ('h3')
  placarFinal.appendChild(msgFinal)
  if (pUser == pComputer){
    msgFinal.innerText = 'Terminou empatado!'
    msgFinal.style.color = 'blue'
  }else if(pUser < pComputer){
    msgFinal.innerText = 'Você perdeu!'
    msgFinal.style.color = 'red'
  }else{
    msgFinal.innerText = 'Você ganhou, parabéns!'
    msgFinal.style.color = 'green'
  }
  let btNew = document.createElement ('button')
  placarFinal.appendChild(btNew)
  btNew.innerText = 'Novo Jogo'
  btNew.setAttribute('onclick', 'window.location.reload()')
}

