function Traveler(name){
    this.name = name
    this.food = 1
    this.isHealth = true
}

Traveler.prototype = {
    constructor: Traveler,
    hunt: function(){
        this.food += 2
    },
    eat: function(){
        if (this.food > 0){
            this.food -= 1
        }else{
            this.isHealth = false
        }
    }
}

function Wagon(capacity){
    this.capacity = capacity
    this.passengers = []
}

Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function(){
        return this.capacity - this.passengers.length
    },
    join: function(traveler){
        if (this.capacity - this.passengers.length > 0){
            this.passengers.push(traveler)
        }
    },
    shouldQuarantine: function(){
        return this.passengers.some(function(passageiro){
            return passageiro.isHealth == false
        })
    },
    totalFood: function(){
        let total = 0
        for (let i in this.passengers){
            total += this.passengers[i].food 
        }
        return total
    }
}

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);