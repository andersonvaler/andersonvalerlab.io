function snapCrackle (maxValue){
    let stringFinal = ""
    for (let i = 1; i <= maxValue; i++){
        if (i % 2 === 1 && i % 5 === 0){
            stringFinal += "SnapCrackle, "                  
        }else if (i % 2 === 1){
            stringFinal += "Snap, " 
        }else if (i % 5 === 0){
            stringFinal += "Crackle, " 
        }else{
            stringFinal += i + ", "
        }   
    }
    console.log(stringFinal)
}
snapCrackle()

function isPrime(num){
    let count = 0
    for (let i = 1; i <= num; i++){
        if (num % i === 0){
            count++
        }
    }
    if (count < 3 && num >= 2){
        return ('Prime')
    }else{
        return('')
    }
}

function snapCracklePrime (maxValue){
    let stringFinal = ""
    for (let i = 1; i <= maxValue; i++){
        if (i % 2 === 1 && i % 5 === 0){
            stringFinal += `SnapCrackle${isPrime(i)}, `                  
        }else if (i % 2 === 1){
            stringFinal += `Snap${isPrime(i)}, ` 
        }else if (i % 5 === 0){
            stringFinal += `Crackle${isPrime(i)}, ` 
        }else{
            if (isPrime(i) == 'Prime'){
                stringFinal += isPrime(i) + ", "  
            }else{
                stringFinal += i + ", "
            } 
        }   
    }
    console.log(stringFinal)
}
snapCracklePrime()